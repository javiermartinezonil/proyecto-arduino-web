<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i = 0; $i < 10; $i++) {

            if ($i % 2 == 0){
                $user = 'Javier Martínez';
            }else{
                $user = 'Leire Martínez';
            }

            if (($i + 1)  % 2 == 0){
                $action = 'close';
            }else{
                $action = 'open';
            }

            DB::table('history')->insert([
                'user' => $user,
                'device_changed' => 'puerta_principal',
                'action' => $action,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }

    }
}
