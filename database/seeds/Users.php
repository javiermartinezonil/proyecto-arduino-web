<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "El papa",
            'email' => "do@mingo.es",
            'role' => "user",
            'password' => bcrypt('1980'),
        ]);

        DB::table('users')->insert([
            'name' => "La mama",
            'email' => "vanema81@gmail.com",
            'role' => "user",
            'password' => bcrypt('1981'),
        ]);

        DB::table('users')->insert([
            'name' => "Leire",
            'email' => "leiremartimor@gmail.com",
            'role' => "user",
            'password' => bcrypt('2007'),
        ]);

        DB::table('users')->insert([
            'name' => "Javi",
            'email' => "jarabito11@gmail.com",
            'role' => "admin",
            'password' => bcrypt('1997'),
        ]);
    }
}
