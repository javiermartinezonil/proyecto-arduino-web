<?php


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/devices', 'DevicesController@getDevices');
Route::get('/history', 'HomeController@getHistory');


//CAMBIAR DISPOSITIVOS
Route::post('/changeData', 'DevicesController@postChangeData');
Route::get('/changeData', 'DevicesController@getChangeData');
