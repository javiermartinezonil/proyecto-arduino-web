<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/changeData', 'API\ApiController@postChangeData');
Route::get('/changeData', 'API\ApiController@getChangeData');
Route::post('/changeDevices', 'API\ApiController@postChangeDevices');



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'api'], function () {
    Route::post('/history', 'API\ApiController@getHistory');

});

