@extends('layouts.default')

@section('content')

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">Dispositivos</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Inicio</a>
                                </li>
                                <li class="breadcrumb-item active">Dispositivos
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">

                @foreach($devices as $i=>$device)
                    <section class="basic-inputs">
                        <div class="row match-height">
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{$device}}</h4>
                                    </div>
                                    <div class="card-block">
                                        <div class="card-body">
                                            <fieldset class="form-group text-center">

                                                <form class="form-horizontal push-10-t" action="/changeData"
                                                      method="POST">
                                                    {{ csrf_field() }}

                                                    <input type="hidden" name="pass" value="12345678">
                                                    <input type="hidden" name="device" value="{{$i}}">

                                                    <button type="submit" class="btn mr-1 mb-1 btn-success btn-lg"><i
                                                            class="ft-power"></i>
                                                    </button>

                                                </form>

                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                @endforeach

            </div>
        </div>
    </div>
@endsection
