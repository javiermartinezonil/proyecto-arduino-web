@extends('layouts.default')

@section('content')

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">Hola {{\Illuminate\Support\Facades\Auth::user()->name}}</h3>
                </div>

            </div>
            <div class="content-body">


                <section class="basic-inputs">
                    <div class="row match-height">
                        <div class="col-xl-12 col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">¿Que quieres hacer?</h4>
                                </div>
                                <div class="card-block">
                                    <div class="card-body">
                                        <fieldset class="form-group text-center">
                                            <a href="/devices" type="button" class="btn mr-1 mb-1 btn-success btn-lg">Dispositivos</a>
                                            <a href="/history" type="button" class="btn mr-1 mb-1 btn-blue btn-lg">Historial</a>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


            </div>
        </div>
    </div>
@endsection
