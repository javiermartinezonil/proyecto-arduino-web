<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    @include('includes.head')
</head>
<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click"
      data-menu="vertical-menu" data-color="bg-chartbg" data-col="2-columns">

<!-- fixed-top-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="collapse navbar-collapse show" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
{{--                    <li class="nav-item d-block d-md-none"><a class="nav-link nav-menu-main menu-toggle hidden-xs"--}}
{{--                                                              href="#"><i class="ft-menu"></i></a></li>--}}
{{--                    <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i--}}
{{--                                class="ficon ft-maximize"></i></a></li>--}}
{{--                    <li class="nav-item dropdown navbar-search"><a class="nav-link dropdown-toggle hide"--}}
{{--                                                                   data-toggle="dropdown" href="#"><i--}}
{{--                                class="ficon ft-search"></i></a>--}}
{{--                        <ul class="dropdown-menu">--}}
{{--                            <li class="arrow_box">--}}
{{--                                <form>--}}
{{--                                    <div class="input-group search-box">--}}
{{--                                        <div class="position-relative has-icon-right full-width">--}}
{{--                                            <input class="form-control" id="search" type="text"--}}
{{--                                                   placeholder="Search here...">--}}
{{--                                            <div class="form-control-position navbar-search-close"><i class="ft-x"> </i>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </form>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
                </ul>

                <ul class="nav navbar-nav float-right">

                    <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link"
                                                                   href="#" data-toggle="dropdown"> <span
                                class="avatar avatar-online"><img
                                    src="https://image.flaticon.com/icons/png/512/149/149071.png" alt="avatar"><i></i></span></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="arrow_box_right"><a class="dropdown-item" href="#"><span
                                        class="avatar avatar-online"><img
                                            src="https://image.flaticon.com/icons/png/512/149/149071.png" alt="avatar"><span
                                            class="user-name text-bold-700 ml-1">{{\Illuminate\Support\Facades\Auth::user()->name}}</span></span></a>
                                <div class="dropdown-divider"></div>
{{--                                <a class="dropdown-item" href="#"><i class="ft-user"></i> Edit Profile</a><a--}}
{{--                                    class="dropdown-item" href="#"><i class="ft-mail"></i> My Inbox</a><a--}}
{{--                                    class="dropdown-item" href="#"><i class="ft-check-square"></i> Task</a><a--}}
{{--                                    class="dropdown-item" href="#"><i class="ft-message-square"></i> Chats</a>--}}
{{--                                <div class="dropdown-divider"></div>--}}
                                <a class="dropdown-item" href="/logout"><i class="ft-power"></i> Cerrar sesión</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<!-- ////////////////////////////////////////////////////////////////////////////-->

<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow " data-scroll-to-active="true"
     data-img="/theme-assets/images/backgrounds/02.jpg">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="/"><img class="brand-logo"
                                                                                        alt="LaBase"
                                                                                        src="/theme-assets/images/LaBaseKeyT.svg"/>
                    <h3 class="brand-text">La Base</h3></a></li>
            <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
    </div>
    @include('includes.menu')
</div>

script

@yield('content')

@include('includes.footer')

</body>
</html>
