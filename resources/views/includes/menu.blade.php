<div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        <li @if( $_SERVER["REQUEST_URI"] == "/") class="active" @else class="nav-item" @endif ><a href="/">
                <i class="ft-home"></i>
                <span class="menu-title" data-i18n="">Inicio</span></a>
        </li>
        <li @if( $_SERVER["REQUEST_URI"] == "/devices") class="active" @else class="nav-item" @endif ><a href="/devices">
                <i class="ft-tablet"></i>
                <span class="menu-title" data-i18n="">Dispositivos</span></a>
        </li>

        <li @if( $_SERVER["REQUEST_URI"] == "/history") class="active" @else class="nav-item" @endif ><a href="/history">
                <i class="ft-book"></i>
                <span class="menu-title" data-i18n="">Historial</span></a>
        </li>
    </ul>
</div>

<div class="navigation-background"></div>
