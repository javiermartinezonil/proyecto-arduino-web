<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Setting;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{

    private $pass=12345678;
    const nameDevice = "arduino_1";

    public function postChangeData(Request $request){

        $signal=$request->device;
        $pass=$request->pass;
        $user=$request->user;
        $action=$request->action;

        if ($pass != $this->pass){
            return response()->json(['message' => "Código incorrecto."], 400);
        }

        DB::table('settings')
            ->where('name', self::nameDevice)
            ->update(['signal' => $signal]);

//        $res = Setting::where('name', self::nameDevice)->first();

//        return response()->json(1, 500);


        $date = new DateTime();
        $date->format("Y-m-d H:i:s");
        $date->modify('+1 hours');


        //GUARDAR EN EL HISTORIAL
        DB::table('history')->insert([
            'user' => $user,
            'device_changed' => " ($signal)",
            'created_at' => $date,
            'action' => $action
        ]);

        return response()->json(1, 200);

    }

    public function getChangeData(Request $request){
//        $device=$request->device;
//        $pass=$request->pass;

        $changed=DB::table('settings')
            ->where('name', self::nameDevice)->first();


        if ($changed == null){
            return response()->json(0, 200);
        }

        if ($changed->signal > 0){
            //EN CASO DE QUE HAYA CAMBIADO
            DB::table('settings')
                ->where('name', self::nameDevice)
                ->update(['signal' => 0]);


            return response()->json($changed->signal, 200);
        }

        return response()->json(0, 200);
    }

    public function getHistory(Request $request){
        $pass=$request->pass;

        if ($pass != $this->pass){
            return response()->json(['message' => "Código incorrecto."], 400);
        }

        $history = DB::table('history')
            ->orderBy('id', 'desc')
            ->get();

        return response()->json([
            "history" => $history
        ], 200);
    }

    public function postChangeDevices(Request $request){
        $devices = $request->device1 . ";" . $request->device2 . ";" . $request->device3 . ";" . $request->device4;

        DB::table('settings')
            ->where('name', self::nameDevice)
            ->update(['devices' => $devices]);

        return response()->json(1, 200);
    }
}
