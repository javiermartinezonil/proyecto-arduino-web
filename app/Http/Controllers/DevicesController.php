<?php

namespace App\Http\Controllers;

use App\Setting;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DevicesController extends Controller
{

    private $pass=12345678;


    public function getDevices(){
        $setting = Setting::where('name', 'arduino_1')->first();
        $devices = explode(';', $setting->devices);
        $array = [];
        foreach ($devices as $d){
            if ($d != ""){
                array_push($array, $d);
            }
        }
        return view("control.control", [
            "devices" => $array
        ]);
    }

    public function postChangeData(Request $request){

        $device=$request->device;
        $pass=$request->pass;

        $res=DB::table('settings')
            ->where('name', "arduino_1")
            ->update(['signal' => $device +1]);

        //GUARDAR EL HISTORIAL
        DB::table('history')->insert([
            'user' => Auth::user()->name,
            'device_changed' => $device,
            'action' => "open",
            'created_at' => date("Y-m-d H:i:s")
        ]);

        return redirect('/devices');
    }

    public function getChangeData(Request $request){

        $device=$request->device;
        $pass=$request->pass;

        $changed=DB::table('settings')
            ->where('name', $device)->first();


            //EN CASO DE QUE HAYA CAMBIADO
            DB::table('settings')
                ->where('name', $device)
                ->update(['value' => false]);

        return redirect('/devices');


    }
}
